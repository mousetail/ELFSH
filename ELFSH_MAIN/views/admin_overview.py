from django.http.response import HttpResponseNotAllowed
from django.shortcuts import render

from ELFSH_MAIN.util.decorators.login_decorators import require_login
from ELFSH_MAIN.models import House, HouseMembership
import django.utils.timezone


@require_login
def admin_overview(request):
    if not request.user.is_staff:
        return HttpResponseNotAllowed("You need to be staff")

    houses = House.objects.all()
    members = [
        {"name": house.name,
         "id": house.id,
         "members": [
             {"name": i.user.username,
              "id": i.user.id,
              "first_login": i.user.date_joined,
              "last_login": i.user.last_login,
              "active": i.user.last_login and (
                      i.user.last_login > (django.utils.timezone.now() - django.utils.timezone.timedelta(days=31)))
              } for i in house.users.all()]
         } for house in houses]
    for member in members:
        member["active"] = len(member["members"]) >= 2 and any(j["active"] for j in member["members"])
    members.sort(key=lambda i: i["active"], reverse=True)

    return render(request, "admin_page.html", {
        "houses": members,
        "nrof_houses": len([i for i in members if i["active"]])
    })
