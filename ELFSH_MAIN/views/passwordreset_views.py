from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN import models
from django.contrib.auth import models as authmodels
from django.contrib.auth.password_validation import validate_password, ValidationError
from django.conf import settings

from django.core.mail import send_mail

from django.http.response import (JsonResponse, HttpResponseBadRequest)
from django.views.decorators.http import require_POST, require_safe
from ELFSH_MAIN.util.decorators import validation, login_decorators
from ELFSH_MAIN.schemas import passwordreset_schema


@require_POST
@validation.validate(
    passwordreset_schema.request_password_reset_schema
)
def request_password_reset(request, data):
    if request.user.is_authenticated:
        return HttpResponseBadRequest("Already logged in")
    email = data["email"]

    try:
        user = authmodels.User.objects.get(email__iexact=email)
    except authmodels.User.DoesNotExist:
        return HttpResponseBadRequest("No user with that email found")

    try:
        token = models.PasswordResetToken.objects.get(user=user)
    except models.PasswordResetToken.DoesNotExist:
        token = models.PasswordResetToken(user=user)
        token.save()

    send_mail(
        localize(request, "reset_email_subject"),
        localize(request, "reset_email_content").format(
            name=user.username,
            link='http://'+settings.HOST + "/"+request.COOKIES.get('lang', 'en')+"/reset/" + str(token.id)),
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False
    )

    return JsonResponse(
        {"status": "sent"}
    )


@require_safe
def verify_reset_token(request, token):
    try:
        token = models.PasswordResetToken.objects.get(id=token)
    except models.PasswordResetToken.DoesNotExist:
        return HttpResponseBadRequest("No token found: " + token)
    except ValidationError:
        return HttpResponseBadRequest("Invalid token: "+token)

    return JsonResponse(
        {
            "user": token.user.username,
        }
    )


@require_POST
@validation.validate(
    passwordreset_schema.apply_reset_token_schema
)
def apply_reset_token(request, token, data):
    try:
        token = models.PasswordResetToken.objects.get(id=token)
    except models.PasswordResetToken.DoesNotExist:
        return HttpResponseBadRequest("No token found")
    except ValidationError:
        return HttpResponseBadRequest("Invalid token: "+token)

    user = token.user

    try:
        validate_password(data["password"])
    except ValidationError as ex:
        return HttpResponseBadRequest(*ex.args)

    user.set_password(data["password"])

    token.delete()

    user.save()

    return JsonResponse(
        {
            "success": True
        }
    )
