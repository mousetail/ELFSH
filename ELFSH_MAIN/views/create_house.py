import random
import base64
import smtplib

from django.db import IntegrityError
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseServerError
from django.conf import settings
from django.views.decorators.http import require_POST, require_safe
from string import ascii_uppercase
from django.contrib.auth import models as authmodels, login
from django.core.exceptions import ValidationError
from django.core.mail import send_mail

from ELFSH_MAIN.util.decorators import validation
from ELFSH_MAIN.schemas import create_house_schemas
from ELFSH_MAIN import models

from ELFSH_MAIN.util.captcha import captcha_image
from ELFSH_MAIN.util.localization import localize


@require_POST
@validation.validate(
    create_house_schemas.create_house_schema
)
def create_user(request, data):
    if request.user.is_authenticated:
        if "userinfo" in data:
            return HttpResponseBadRequest('Cannot create account while logged in')
    else:
        if "userinfo" not in data:
            return HttpResponseBadRequest('Must create account')

    try:
        captcha = models.CaptchaRecord.objects.get(
            id=data["captcha_id"],
            value__iexact=data["captcha"].lower()
        )
    except models.CaptchaRecord.DoesNotExist:
        return HttpResponseBadRequest(
            "Capcha not correct"
        )

    if not request.user.is_authenticated:
        try:
            authmodels.User.objects.get(
                username=data["userinfo"]["username"].strip(),
            )
            return HttpResponseBadRequest(
                "Username already exists"
            )
        except authmodels.User.DoesNotExist:
            pass

        try:
            authmodels.User.objects.get(
                email=data["userinfo"]["email"]
            )
            return HttpResponseBadRequest(
                "Email not unique"
            )
        except authmodels.User.DoesNotExist:
            pass

        user = authmodels.User.objects.create_user(
            username=data["userinfo"]["username"].strip(),
            password=data["userinfo"]["password"],
            email=data["userinfo"]["email"],
            is_active=False,
        )
        user.save()

        elfshuser = models.ELFSHUser(
            user=user,
        )
        elfshuser.save()

        verification = models.AccountEmailConfirmation(
            houseName=data["name"],
            user=elfshuser,
        )
        verification.save()
        try:
            send_mail(
                localize(request, 'verification_email_subject'),
                localize(request, 'verification_email_body').format(
                    name=data["userinfo"]["username"],
                    link='http://' + settings.HOST + '/' + request.COOKIES.get('lang', 'en') + '/verify/' + str(
                        verification.id)
                ),
                settings.EMAIL_HOST_USER,
                [data["userinfo"]["email"]]
            )
        except smtplib.SMTPException as ex:
            return HttpResponseServerError("Failed to send email: ", *ex.args)

        captcha.delete()

        return JsonResponse({"reply_email": settings.EMAIL_HOST_USER})
    else:
        elfshuser = models.ELFSHUser.objects.get(user=request.user)

        house = models.House(
            name=data["name"],
        )
        house.save()

        membership = models.HouseMembership(
            user=elfshuser,
            house=house,
        )
        membership.save()

        captcha.delete()

        return JsonResponse(
            {
                "house_id": house.id,
                "user_id": request.user.id
            }
        )


@require_safe
def generate_captcha(request):
    excluded_letters = 'ilo'

    upper = tuple(i for i in ascii_uppercase if i not in excluded_letters)

    text = ''.join(random.choice(upper) for i in range(random.randint(5, 10)))

    data = captcha_image(text)

    record = models.CaptchaRecord(
        value=text
    )
    record.save()

    return JsonResponse(
        {
            "id": record.id,
            "data": base64.urlsafe_b64encode(data).decode("utf-8")
        }
    )


@require_POST
@validation.validate(
    create_house_schemas.validate_email_schema
)
def verify_email(request, invitation_id, data={}):
    try:
        confirmation = models.AccountEmailConfirmation.objects.get(
            id=invitation_id
        )
    except models.AccountEmailConfirmation.DoesNotExist:
        return HttpResponseBadRequest(localize(request, 'verification_error_used'))
    except ValidationError:
        return HttpResponseBadRequest(localize(request, 'verification_error_invalid'))

    house = models.House(
        name=confirmation.houseName
    )
    house.save()

    membership = models.HouseMembership(
        house=house,
        user=confirmation.user
    )
    membership.save()

    confirmation.user.user.is_active = True
    confirmation.user.user.save()

    login(
        request,
        confirmation.user.user
    )

    confirmation.delete()

    return JsonResponse(
        {
            "user_id": confirmation.user.user.id,
            "house_id": house.id,

            "user": {
                "username": confirmation.user.user.username,
                "id": confirmation.user.user.id,
                "email": confirmation.user.user.email,
                "is_staff": confirmation.user.user.is_staff
            }
        }
    )
