import uuid

from django.db import models
from django.contrib.auth import models as authmodels

from django.utils import timezone


# Create your models here.
class House(models.Model):
    name = models.CharField(max_length=64)
    closingTime = models.TextField(max_length=6, default="18:00")  # storing as a string
    editableDays = models.IntegerField(default=14)
    timeZone = models.TextField(max_length=32,
                                default='Europe/Amsterdam'
                                )  # longest timezone name is 'America/Argentina/Buenos_Aires' (30 chars)

    # will only be used client side anyway
    # clozingTime = models.TimeField(default=timezone.time.)

    def __str__(self):
        return self.name


class ELFSHUser(models.Model):
    user = models.OneToOneField(authmodels.User, on_delete=models.CASCADE)
    houses = models.ManyToManyField(House, related_name="users", through='HouseMembership')
    language = models.CharField(max_length=2)

    def __str__(self):
        return self.user.username


class HouseMembership(models.Model):
    user = models.ForeignKey(ELFSHUser, on_delete=models.CASCADE, db_index=True)
    house = models.ForeignKey(House, on_delete=models.CASCADE, db_index=True)
    active = models.BooleanField(default=True)

    money = models.IntegerField(default=0)  # Money in integer number of cents
    points = models.IntegerField(default=0)  # Cooking points

    def __str__(self):
        return self.user.user.username + ">" + self.house.name + "(" + str(self.points) + ", €" + str(
            self.money / 100.0) + ")"


class EatState(models.Model):
    house = models.ForeignKey(House, on_delete=models.CASCADE)
    user = models.ForeignKey(ELFSHUser, on_delete=models.SET_NULL, null=True)
    date = models.DateField()
    eatState = models.IntegerField(
        choices=((1, 'Cooking'),
                 (2, 'Eating'))
    )
    extraPeople = models.IntegerField(default=0)


class Expense(models.Model):
    description = models.CharField(max_length=256)
    house = models.ForeignKey(House, on_delete=models.CASCADE)
    payed_by = models.ForeignKey(ELFSHUser, related_name="expenses_payed", on_delete=models.SET_NULL, null=True)
    payed_for = models.ManyToManyField(ELFSHUser,
                                       related_name="expenses_payed_for",
                                       through="ExpensePayedFor")
    cost = models.IntegerField()  # Integer number of cents
    date = models.DateField()
    date_modified = models.DateField(auto_now=True)

    is_meal = models.BooleanField(default=False)

    deleted = models.BooleanField(default=False)

    def apply_expense(self, all_memberships=None, save=True, invert=False):
        if self.deleted:
            return  # a deleted expense has no effect

        if all_memberships is None:
            payed_for = tuple(self.payed_for.all())
            all_memberships = [
                HouseMembership.objects.get(house=self.house, user=i)
                for i in payed_for
            ]
            if self.payed_by not in payed_for:
                all_memberships.append(HouseMembership.objects.get(house=self.house, user=self.payed_by))

        cost_pp = self.get_costs_per_person()
        for model in all_memberships:
            try:
                current_cost = next(i for i in cost_pp if i["id"] == model.user.user_id)
            except StopIteration:
                # This means that there is a extra model in all models
                # that does not appear in cost_pp
                # It can be ignored
                pass
            else:
                model.money += current_cost["cost"] * (-1 if invert else 1)
                model.points += current_cost["points"] * (-1 if invert else 1)

                if save:
                    model.save()

    def revert_expense(self, all_models=None, save=True):
        self.apply_expense(all_models, save, True)

    def get_costs_per_person(self):
        # seems to work with a negative amount of people as well
        # +1 to how pythons floordiv works
        expense_paid_for_membership = ExpensePayedFor.objects.filter(expense=self)
        total_number = sum(1 + i.extra for i in expense_paid_for_membership)

        if total_number == 0:
            cost_pp = 0
            cost_extra = self.cost
        else:
            cost_pp = self.cost // total_number
            cost_extra = self.cost % total_number

        out = [{
            "name": paidFor.user.user.username,
            "id": paidFor.user.user_id,
            "cost": -cost_pp * (paidFor.extra + 1),
            "points": 0,
            "extra": paidFor.extra
        } for paidFor in expense_paid_for_membership]

        for i, ind in enumerate(out):
            for j, ind2 in enumerate(out):
                if i != j and ind["id"] == ind2["id"]:
                    self.is_meal = False
                    self.deleted = True
                    self.save()
                    return {}
                    # assert ind["id"] != ind2["id"], str(self.date) + str(self.description)

        i = 0
        while cost_extra > 0 and total_number > 0:
            value = min(cost_extra, out[i]["extra"] + 1)
            out[i]["cost"] -= value  # distribute the extra cents
            cost_extra -= value
            i += 1
            if i >= len(out):
                i = 0

        if self.payed_by.user_id not in [i["id"] for i in out]:
            out.append({"name": self.payed_by.user.username,
                        "id": self.payed_by.user_id,
                        "cost": 0,
                        "points": 0,
                        "extra": -1})
            added = True
        else:
            added = False
        own_pair_for = next(i for i in out if i["id"] == self.payed_by.user_id)
        if total_number > 0:
            own_pair_for["cost"] += self.cost

        if self.is_meal:
            for i in out:
                if i["id"] == self.payed_by.user_id:
                    i["points"] += total_number
                i["points"] -= i["extra"] + 1

        assert sum(i["points"] for i in out) == 0, ("Points: " + (
            ",".join(str(i["points"]) for i in out)) + " Total: " + str(total_number) +
                                                    ", added: " + str(added) + " locals: " + str(locals()))

        return out


class ExpensePayedFor(models.Model):
    expense = models.ForeignKey(Expense, on_delete=models.CASCADE,
                                db_index=True,
                                related_name="expense_payed_for_objects")
    user = models.ForeignKey(ELFSHUser, on_delete=models.CASCADE,
                             related_name="user_payed_for_objects",
                             db_index=True)
    extra = models.IntegerField(default=0)


class Invitation(models.Model):
    name = models.TextField(max_length=20)
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    email = models.TextField()
    house = models.ForeignKey(House, on_delete=models.CASCADE)


class PasswordResetToken(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    user = models.ForeignKey(to=authmodels.User, on_delete=models.CASCADE)
    createdDate = models.DateField(auto_now_add=True)


class NotificationProcessed(models.Model):
    house = models.ForeignKey(to=House, on_delete=models.CASCADE)
    date = models.DateField()


class CaptchaRecord(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    value = models.CharField(editable=False, max_length=20)
    modifiedDate = models.DateField(auto_now_add=True, editable=False)


class AccountEmailConfirmation(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    houseName = models.CharField(editable=False, max_length=64)
    user = models.ForeignKey(to=ELFSHUser, on_delete=models.CASCADE,
                             editable=False)
    modifiedDate = models.DateField(auto_now_add=True, editable=False)
