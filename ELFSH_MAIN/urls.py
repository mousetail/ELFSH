from django.conf.urls import url

import ELFSH_MAIN.views.cost_views
import ELFSH_MAIN.views.days_views
import ELFSH_MAIN.views.staticfile_views
import ELFSH_MAIN.views.user_views
from ELFSH_MAIN.views import misc_views, passwordreset_views, invitation, push, create_house, admin_overview, errorviews

urlpatterns = [
    url(r"^api/future_days/([0-9]*)/?", ELFSH_MAIN.views.days_views.future_days),
    url(r"^api/costs/([0-9]*)/?", ELFSH_MAIN.views.cost_views.get_costs),
    url(r"^api/update/([0-9]*)/([0-9]*)/([0-9]*)/([0-9]*)/([0-9]*)/?$", ELFSH_MAIN.views.days_views.insert_day),
    url(r"^api/new/([0-9]*)/?$", ELFSH_MAIN.views.cost_views.add_cost),
    url(r"^api/info/([0-9]*)/?$", misc_views.house_info),
    url(r"^api/user/([0-9]*)/delete/([0-9]*)/?$", ELFSH_MAIN.views.user_views.delete_user),
    url(r"^api/set_house_info/([0-9]*)/?$", misc_views.set_house_info),
    url(r"^api/send_invitation/([0-9]*)/?$", invitation.send_invitation),
    url(r"^api/verify_invitation/?$", invitation.verify_invitation),
    url(r"^api/create_account/?$", invitation.register),
    url(r"^api/accept_invitation/?$", invitation.accept_invitation),
    url(r"^api/login/?$", ELFSH_MAIN.views.user_views.login_view),
    url(r"^api/logout/?$", ELFSH_MAIN.views.user_views.logout_view),
    url(r"^api/current_user_info/?", ELFSH_MAIN.views.user_views.get_current_user_info),
    url(r"^api/houses/?$", misc_views.get_houses),
    url(r"^api/reset/request/?$", passwordreset_views.request_password_reset),
    url(r"^api/reset/verify/(.*)?$", passwordreset_views.verify_reset_token),
    url(r"^api/reset/apply/(.*)?$", passwordreset_views.apply_reset_token),
    url(r"^api/subscribe/?$", push.registerPush),
    url(r"^api/captcha/?$", create_house.generate_captcha),
    url(r"^api/new_house/?$", create_house.create_user),
    url(r"^api/verify/(.*)/?$", create_house.verify_email),
    url(r"^api/export/([0-9]+)/(.+)/?$", ELFSH_MAIN.views.cost_views.get_cost_as_csv),
    url(r"^api/", errorviews.return_404),
    url(r"^serviceworker\.js$", ELFSH_MAIN.views.staticfile_views.serviceworker),
    url(r"^robots\.txt$", ELFSH_MAIN.views.staticfile_views.robots),
    url(r"^favicon\.ico$", ELFSH_MAIN.views.staticfile_views.favicon),
    url(r"^.+\.txt$", errorviews.return_404),
    url(r"^.+\.xml$", errorviews.return_404),
    url(r"^.+\.html?$", errorviews.return_404),
    url(r"^secret_admin_page", ELFSH_MAIN.views.admin_overview.admin_overview),
    # all other pages are handled by react-router
    url(r'^', misc_views.main_page)
]
