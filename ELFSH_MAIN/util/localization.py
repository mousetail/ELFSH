from django.http import HttpRequest

from typing import Optional

localizations = {
    "formatting_json_invalid": {
        "en": "Data must be in valid JSON format",
        "nl": "Data moet in het JSON formaat worden gegeven"
    },
    "formatting_json_bad_root": {
        "en": "JSON data must be a dictionary",
        "nl": "De JSON data moet met een dict beginnen"
    },
    "formatting_missing_parameter": {
        "en": "Missing parameter {}, required parameters: {}",
        "nl": "De parameter {} ontbreekts, alle verplichte parameters: {}"
    },
    "formatting_type_invalid": {
        "en": "Invalid type for parameter {0}, should be {1}, was {2}",
        "nl": "Verkeerd type voor paremter {0}, was {2} in plaats van {1}"
    },
    "formatting_string_invalid": {
        "en": "Parameter {0} should match regular expression {1}",
        "nl": "Parameter {0} moet voldoen aan het reguliere expressie {1}"
    },
    "formatting_number_range": {
        "en": "{0} should be in between {1} and {2}",
        "nl": "{0} moet tussen de {1} en de {2} zitten"
    },
    # For home loading page
    "loading_loading": {
        "en": "Loading...",
        "nl": "Laden..."
    },
    "loading_desc": {
        "en": "If this takes more than a few seconds, check the following:",
        "nl": "Als dit meer dan een paar seconden duurt, controleer het volgende:"
    },
    "loading_1": {
        "en": "Is your internet connection working?",
        "nl": "Werkt je internet verbinding?"
    },
    "loading_2": {
        "en": "Are you using a recent version of Firefox, Edge or Chrome?",
        "nl": "Gebruik je een recente versie van Firefox, Edge of Chrome?"
    },
    "loading_3": {
        "en": "Are there any errors in the console?",
        "nl": "Staan er een paar errors in the console?"
    },
    # for errors
    "error_400": {
        "en": "Bad request: {0}",
        "nl": "Verzoek niet begrepen: {0}"
    },
    "error_403": {
        "en": "Not authorised, please login again",
        "nl": "Niet geauthoriseert, probeer opniew in te loggen"
    },

    "error_csrf": {
        "en": "{0}\n Please make sure cookies are enabled. If the problem"
              " persists, please stop using Microsoft Edge.",
        "nl": "{0}\n Zorg dat cookies aan staan. Als dit niet werkt, probeer een andere brouwser. Microsoft Edge"
              " specefiek zorgt soms voor problemen."
    },

    "error_404": {
        "en": "Page not found. {0}",
        "nl": "Pagina niet gevonden. {0}"
    },

    "error_500": {
        "en": "Internal server error. Please contact me if this happens more frequently.",
        "nl": "Interne fout. Neem contact op als dit vaker gebeurt."
    },

    "login_required": {
        "en": "Login Required",
        "nl": "Inloggen vereist"
    },
    "login_invalid": {
        "en": "Invalid username or password",
        "nl": "Login gegevens niet correct"
    },
    "login_unauthorised": {
        "en": "You are not authorised",
        "nl": "U bent hier niet toe bevoegd"
    },

    "cost_payees_not_empty": {
        "en": "At least one participant required",
        "nl": "Ten minste één deelnemer vereist",
    },
    "cost_description_not_empty": {
        "en": "Description may not be emtpy",
        "nl": "Beschrijving mag niet leeg zijn"
    },
    "cost_does_not_exist": {
        "en": "Cost with ID {0} does not exist",
        "nl": "Kosten met een ID van {0} bestaat niet",
    },

    "list_bad_state": {
        "en": "Invalid eat status: ",
        "nl": "Ongeldige eetstatus",
    },
    "list_bad_extra": {
        "en": "Invalid \"extra eat\", must be integer and at least 0",
        "nl": "Ongeldige extra eters, moet een geheel positief getal zijn"
    },
    "list_bad_date": {
        "en": "Date should be between now and 15 days in the future",
        "nl": "Datum moet tussen nu en over 15 dagen zijn",
    },

    # for email
    "email_invite_subject": {
        "en": "You have been invited to ELFSH",
        "nl": "U bent uitgenodigd voor ELFSH"
    },
    "email_invite_content": {
        "en": """
            Dear {0},
            
            You have been invited join the house {1} on ELFSH.
            ELFSH is a system to keep track of who eats when and how much it costs.
            
            Click the link to accept:
            {2}
        """,
        "nl": """
            Beste {0},
            
            U bent uitgenodigd om deel te nemen aan {1} on ELFSH.
            ELFSH is een system om bij te houden wie wanneer mee eet en hoeveel dat gekost heeft.
            
            Klik op de link hieronder om de accepteren:
            {2}
        """,

    },
    "house_bad_name": {
        "en": "Name must not be too long",
        "nl": "Naam mag niet te lang zijn"
    },

    "reset_email_subject": {
        "en": "Password reset link for ELFSH",
        "nl": "Watchtwoord reset link voor ELFSH",
    },
    "reset_email_content": {
        "en": """
        Dear {name}
        
        There was a request to reset your password.
        Click on the link below to create a new password:
        {link}
        """,
        "nl": """
        Beste {name},
        
        Er is zojuist een verzoek verstuur om u watchwoord te resetten.
        Klik op deze link om een niew wachtwoord in te stellen.
        {link}"""
    },
    "push_test_title": {
        "en": "Test notificatie",
        "nl": "Test notificatie"
    },
    "push_test_message": {
        "en": "Congradulations, if you see this it means notifications are working.",
        "nl": "Gefeliciteerd, als u dit ziet betekent het hat notificaties werken!"
    },
    "push_test_tag": {
        "en": "",
        "nl": "",
    },
    "push_cook_message": {
        "en": "Today, you are cooking for {} people",
        "nl": "U kookt vandaag voor {} personen"
    },
    "push_cook_with": {
        "en": "You are cooking together with {}",
        "nl": "U kookt samen met {}"
    },
    "push_cook_and": {
        "en": " and ",
        "nl": " en "
    },

    "push_no_message": {
        "en": "Nobody has volunteered to cook",
        "nl": "Niemand heeft zich ingeschreven om te koken",
    },
    "push_no_title": {
        "en": "ELFSH",
        "nl": "ELFSH"
    },

    # Validation Error Messages
    "validation_closing_time": {
        "en": "Closing time must be in HH:MM format",
        "nl": "Stuitingstijd moet in een HH:MM formaat opgeschreven staan"
    },
    "validation_name_not_empty": {
        "en": "Name may not be empty",
        "nl": "Naam mag niet leeg zijn"
    },
    "validation_email": {
        "en": "Email must be valid",
        "nl": "Vul een correct email adres in"
    },
    "validation_language": {
        "en": "Language must be either \"en\" or \"nl\"",
        "nl": "Taal moet of \"en\" or \"nl\" zijn"
    },
    "validation_username": {
        "en": "Please select a username",
        "nl": "Selecteer een gebruikersnaam"
    },
    "validation_password": {
        "en": "Please enter a password",
        "nl": "Voer een wachtwoord in"
    },
    "verification_email_subject": {
        "en": "Welcome to ELFSH",
        "nl": "Welkom bij ELFSH",
    },
    "verification_email_body": {
        "en": "Dear {name}\n"
              "Welcome to ELFSH! Please click the link below to get started\n"
              "{link}",
        "nl": "Beste {name}\n"
              "Welkom bij ELFSH! Klik de link hier onder om te beginnen:\n"
              "{link}"
    },
    "verification_error_used": {
        "en": "This activation link has probably already been used or has expired.",
        "nl": "Deze activatie link is waarschinlijk al gebruikt of is verlopen. "
    },
    "verification_error_invalid": {
        "en": "This activation link is invalid. Make sure it is copied exactly the same as in the email.",
        "nl": "Deze activatie link in niet geldig. Zorg dat hij precies hetzelfde in ingevuld als in de email."
    }
}


def try_localize(request: Optional[HttpRequest], key: str, lang=None):
    if key in localizations:
        return localize(request, key, lang=lang)
    return key


def localize(request: Optional[HttpRequest], key: str, lang=None):
    if lang is None:
        lang = request.COOKIES.get("lang", "en")
    return localizations[key][lang]
