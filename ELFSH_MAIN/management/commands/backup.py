import subprocess
import tempfile
import json
import secrets
import time
import jwt
import datetime
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key

from django.core.management.base import BaseCommand
from django.conf import settings
import requests


class Command(BaseCommand):
    def get_access_token(self):
        with open('secret/box-keyfile.json', 'r') as f:
            auth = json.load(f)

        key = load_pem_private_key(
            data=auth["boxAppSettings"]["appAuth"]["privateKey"].encode('utf-8'),
            password=auth["boxAppSettings"]["appAuth"]["passphrase"].encode('utf-8'),
            backend=default_backend(),
        )

        authentication_url = 'https://api.box.com/oauth2/token'
        claims = {
            'iss': auth['boxAppSettings']['clientID'],
            'sub': auth['enterpriseID'],
            'box_sub_type': 'enterprise',
            'aud': authentication_url,
            # This is an identifier that helps protect against
            # replay attacks
            'jti': secrets.token_hex(64),
            # We give the assertion a lifetime of 45 seconds
            # before it expires
            'exp': round(time.time()) + 45
        }

        keyId = auth["boxAppSettings"]["appAuth"]["publicKeyID"]
        assertion = jwt.encode(
            claims,
            key,
            algorithm='RS512',
            headers={
                'kid': keyId
            }
        )

        session = requests.Session()
        # session.headers["Content-Type"] = "Application/Json"
        response = session.post(
            authentication_url,
            {
                'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                'assertion': assertion,
                'client_id': auth["boxAppSettings"]["clientID"],
                'client_secret': auth["boxAppSettings"]["clientSecret"]
            }
        )

        result = response.json()
        print(result)
        return result["access_token"]

    def handle(self, *args, **kwargs):
        database = settings.DATABASES["default"]
        if 'postgresql' in database["ENGINE"].lower():
            print("Dumping POSTGRES database")
            file = tempfile.TemporaryFile('w+b')
            process = subprocess.Popen(
                ('pg_dump', '-h', database["HOST"], '-U', database["USER"], '-w', '-Fc', database["NAME"]),
                stdout=file,
                stdin=subprocess.PIPE,
                env={
                    'PGPASSWORD': database["PASSWORD"],
                    'PGHOST': database["HOST"]
                }
            )
            time.sleep(0.1)
            #process.communicate((database["PASSWORD"] + '\n').encode('utf-8'))
            process.wait()
            if process.returncode != 0:
                print("Process exited with exit code " + str(process.returncode))
                return
            file.seek(0)
        elif 'sqlite' in database["ENGINE"].lower():
            print("Dumping SQLITE3 Database")
            file = open(database["NAME"], 'rb')
        else:
            return

        with file:
            token = self.get_access_token()
            session = requests.session()
            session.headers["Authorization"] = "Bearer " + token

            dt = datetime.datetime.now()
            filename = dt.isoformat().replace(' ', '_').replace('/', '_') + '.sqlc'
            print(filename)

            response = session.post(
                'https://upload.box.com/api/2.0/files/content',
                {
                    "attributes": json.dumps(
                        {
                            'name': filename,
                            'parent': {
                                'id': 0
                            }
                        }
                    )
                },
                files=[
                    (filename, file)
                ]
            )
            print(response.json())
            response.raise_for_status()
