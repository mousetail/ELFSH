from django.core.management.base import BaseCommand
from django.conf import settings
import django.apps
from django.contrib.auth import models as authmodels
from ELFSH_MAIN import models


class Command(BaseCommand):
    help = "reset database"

    blacklisted_apps = [
        'contenttypes',
    ]

    def handle(self, *args, **options):
        print(options)

        if not settings.DEBUG:
            print("Refusing to wipe database in production mode")
            return

        print("If you continue, the following items will be deleted:")
        for app_config in django.apps.apps.get_app_configs():
            app_models = app_config.get_models()
            app_models = list(app_models)
            if app_config.label in self.blacklisted_apps:
                continue
            if len(app_models) > 0:
                print("From " + app_config.label)
            for model in app_models:
                length = len(model.objects.all())
                if length > 0:
                    print("\t{: 4} items from {}".format(length, model.__name__))

        if input("Do you want to continue? (y/N)").upper() != "Y":
            print("Aborted")
            return

        for app_config in django.apps.apps.get_app_configs():
            app_models = app_config.get_models()
            app_models = list(app_models)
            if app_config.label in self.blacklisted_apps:
                continue
            for model in app_models:
                model.objects.all().delete()

        print("Database wiped")

        house = models.House(
            name="test house",
        )
        house.save()

        for i in range(10):
            user = authmodels.User.objects.create_user(
                username='user_' + str(i),
                password='password_' + str(i),
                email='user_' + str(i) + '@elfsh.mousetail.nl',
                is_staff=i == 0,
                is_superuser=i == 0
            )
            user.save()
            
            elfshuser = models.ELFSHUser(
                user=user,
            )
            elfshuser.save()

            membership = models.HouseMembership(
                user=elfshuser,
                house=house
            )
            membership.save()

            print("\tCreated user "+str(i))
