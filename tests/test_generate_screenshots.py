from .frontend_test import FrontendTest
import ELFSH_MAIN.models as models
from PIL import Image


class ScreenshotTests(FrontendTest):
    """
    Not actually a test, but I need the test context to generate the screenshots
    """

    def test_go_through_screenshots(self):
        self.subtest_lang("en", [
            "Andy",
            "Bob",
            "Charlie",
            "Deborah",
            "Fred",
            "Gregory",
            "Alice",
            "Tom",
            "Kimberly",
            "Lora",
            "Thomas",
            "Wolfram",
            "Peter"
        ]),
        self.subtest_lang("nl", [
            "Mark",
            "Frits",
            "Anne",
            "Kim",
            "Nienke",
            "Tim",
            "Frederick",
            "Thomas",
            "Leto",
            "Stefan",
            "Suzan",
            "Roy"
        ])

    def save_image(self, lang, name, crop, query=".main-content"):

        self.browser.execute_script(
            "window.scrollTo(0, document.querySelector(\"" + query +
            "\").getBoundingClientRect().top+4)")

        self.browser.save_screenshot("static/images/promo/" + lang + "/" + name + ".png")
        if crop:
            image = Image.open("static/images/promo/" + lang + "/" + name + ".png")

            print("cropping: " + str(image))

            image = image.crop((crop[0], crop[1], crop[0] + crop[2], crop[1] + crop[3]))

            image.save("static/images/promo/" + lang + "/" + name + ".png")

    def subtest_lang(self, lang, names):
        for i, user in enumerate(self.users):
            user.user.username = names[i]
            user.user.save()

        self.browser.get(self.live_server_url + "/" + lang + "/login")
        self.browser.set_window_size(1500, 800)

        self.save_image(lang, "homepage", (350, 0, 800, 370))

        self.waitForElement('#username').send_keys(
            self.users[0].user.username
        )
        self.browser.find_element_by_id('password').send_keys(
            self.passwords[0]
        )
        button = self.browser.find_element_by_css_selector('button[type=submit]')
        print(button.get_attribute('type'), button.get_attribute('class'), button.text)
        button.click()

        self.waitForElement('div.nav')
        table = self.waitForElement('table')
        elements = table.find_elements_by_class_name(
            'table-elem-0'
        )
        length = len(elements)
        print(length)

        indexes = [
            0,  # Person 1 day 1
            2,  # Person 3 day 1
            2,  # Person 3 day 1
            8,  # person 2 day 2
            10,  # person 4 day 2
            20,  # Person 1 day 3
            20,  # Person 1 day 3
            21,  # Person 2 day 3
            22,  # Person 3 Day 3
            23,  # Person 4 Day 3
            27,  # I have officially lost track at this point
            26,  # "
            31,  # "
        ]

        for index in indexes:
            elements[index].click()

        self.save_image(lang, "list2", [160, 155, 582, 355])

        self.browser.find_element_by_id('nav-cost').click()
        self.waitForElement('#nav-cost-div.nav-element-selected')
        self.waitForElement('form')

        self.save_image(lang, "cost2", [160, 135, 582, 355])

        self.save_image(lang, "cost3", [459, 20, 582, 355], 'form')

        self.browser.find_element_by_id('nav-settings').click()
        self.waitForElement('#nav-settings-div.nav-element-selected')

        self.save_image(lang, "user2", [459, 240, 582, 355], 'form:nth-of-type(2)')
