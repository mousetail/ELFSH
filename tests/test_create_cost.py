import json

from .test_views import BasicTestCase
from ELFSH_MAIN import models


class TestCreateCost(BasicTestCase):
    def testAddCost1(self):
        AMOUNT = 101  # €1

        for m1 in range(3):
            for m2 in range(3):
                for m3 in range(3):
                    for payer_index in range(3):
                        h_mem = [
                            models.HouseMembership.objects.get(user=self.users[i])
                            for i in range(3)
                        ]
                        moneys = [i.money for i in h_mem]

                        name = "expense i={0} j={1} k={2} l={3}".format(m1, m2, m3, payer_index)
                        response = self.client.post(
                            "/api/new/" + str(self.house.id),
                            json.dumps({
                                "description": name,
                                "amount": AMOUNT,
                                "paid_by": self.users[payer_index].id,
                                "payed_for": {
                                    self.users[0].user.username: m1,
                                    self.users[1].user.username: m2,
                                    self.users[2].user.username: m3,
                                },
                                "id": 0,
                                "deleted": False,
                                "max": 3 * 3 * 3 * 3,
                                'is_meal': False,
                            }),
                            content_type="application/json"
                        )
                        if m1 + m2 + m3 == 0:
                            self.assertNotEquals(response.status_code, 200, name)
                        else:
                            self.assertEquals(response.status_code, 200, response.content)

                            data = json.loads(response.content)
                            costs = [i for i in data["result"] if i["description"] == name]
                            self.assertEquals(len(costs), 1, repr(name) + " not in " +
                                              ", ".join(i["description"] for i in data["result"]))
                            costs = costs[0]["costs"]

                            costs_s = []

                            for k in range(3):
                                filtered_costs = [i for i in costs if i["id"] == self.users[k].id]
                                if [m1, m2, m3][k] == 0 and k != payer_index:
                                    self.assertEquals(len(filtered_costs), 0, name+", "+json.dumps(filtered_costs))
                                    costs_s.append(None)
                                else:
                                    self.assertEquals(len(filtered_costs), 1)
                                    costs_s.append(filtered_costs[0])

                            total_pp_min = (-AMOUNT) // (m1 + m2 + m3)
                            total_pp_max = total_pp_min + 1

                            if payer_index != 0 and m1 != 0:
                                self.assertGreaterEqual(costs_s[0]["cost"], total_pp_min * m1, name)
                                self.assertLessEqual(costs_s[0]["cost"], total_pp_max * m1, name)
                                self.assertEquals(costs_s[0]["extra"], m1 - 1)

                            if payer_index != 1 and m2 != 0:
                                self.assertGreaterEqual(costs_s[1]["cost"], total_pp_min * m2, name)
                                self.assertLessEqual(costs_s[1]["cost"], total_pp_max * m2, name)
                                self.assertEquals(costs_s[1]["extra"], m2 - 1)

                            if payer_index != 2 and m3 != 0:
                                self.assertGreaterEqual(costs_s[2]["cost"], total_pp_min * m3, name)
                                self.assertLessEqual(costs_s[2]["cost"], total_pp_max * m3, name)
                                self.assertEquals(costs_s[2]["extra"], m3 - 1)

                            for index, money in enumerate(moneys):
                                newmoney = models.HouseMembership.objects.get(id=self.users[index].id).money

                                if costs_s[index] is not None:
                                    self.assertEquals(newmoney - money,
                                                      costs_s[index]["cost"], repr(name) + ", index=" + str(index))
                                else:
                                    self.assertEquals(newmoney, money, name + ", index=" + str(index) + ", costs=" +
                                                      ", ".join(str(i["id"]) + ":" + str(i["cost"]) for i in costs_s if
                                                                i is not None))


