const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const childProcess = require('child_process');

const commit = childProcess.execSync('git log -1 --pretty="format:%h %B"')
  .toString();

module.exports = {
  mode: 'development',
  entry: { // "index":"./assets/js/app",
    main: './assets/js/index',
    serviceworker: './assets/js/serviceworker/serviceworker.js',
    style: './assets/style/style.css',
  },

  output: {
    path: path.resolve('./static/assets/bundles/'),
    filename: '[name]-[hash].js',
    publicPath: '/static/assets/bundles/',
    // globalObject: "this",
  },

  plugins: [
    new BundleTracker(
      { filename: './webpack-stats.json' },
    ),
    new MiniCssExtractPlugin({
      filename: 'style-[hash].css',
    }),
    new webpack.DefinePlugin({
      BUILT_AT: JSON.stringify(new Date().toDateString()),
      COMMIT_MESSAGE: JSON.stringify(commit),
    }),
  ],

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-react'],
          plugins: ['babel-plugin-syntax-dynamic-import'],
        },
      },
      {
        test: /\.css$/i,
        use: [{
          loader: MiniCssExtractPlugin.loader,
        }, 'css-loader'],
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    modules: ['node_modules', 'bower_components'],
    extensions: ['.js', '.jsx'],
    alias: {
      '@fortawesome/fontawesome-free-solid$': '@fortawesome/fontawesome-free-solid/shakable.es.js',
      '@fortawesome/fontawesome-free-regular$': '@fortawesome/fontawesome-free-regular/shakable.es.js',
    },
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    minimizer: [
      new TerserJSPlugin({}),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
};
