import React from 'react';

export class ErrorView extends React.PureComponent {
  render() {
    if (this.props.error === undefined || this.props.error === null) {
      return null;
    }
    let val;

    if (typeof this.props.error === 'string') {
      val = this.props.error;
    } else if (this.props.error instanceof Error) {
      val = this.props.error.message;
    } else if (React.isValidElement(this.props.error)) {
      val = this.props.error;
    }

    return <div className="errorMessage">{val}</div>;
  }
}
