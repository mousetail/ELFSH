import React from 'react';
import { localize } from '../util/localization';

export class ErrorBoundary extends React.Component {
  constructor() {
    super();

    this.state = {
      error: null,
    };
  }

  componentDidCatch(error, info) {
    this.setState({ error, info });
  }

  render() {
    if (this.state.error) {
      return (
        <div className="main-content">
          <h1>{localize('err_internal')}</h1>
          <div className="text-container">
            <p>{localize('err_explain')}</p>

            <h2>{localize('err_details')}</h2>
            <pre>
              {`${this.state.error}`}
              {`${this.state.info.componentStack}`}
            </pre>
            <h2>Stack trace</h2>
            <pre>
              {
                        this.state.error.stack
                    }
            </pre>
          </div>
        </div>
      );
    }

    return this.props.children;
  }
}
