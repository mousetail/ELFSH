import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatMoney } from '../../util/localization';

export class ExpensePerson extends React.Component {
  constructor() {
    super();
  }

  renderEditing() {
    return (
      <>
        <div className="expense-item-name">{this.props.cost.name}</div>

        <div className="expense-item-point">
          x
          {this.props.cost.extra + 1}
        </div>

        <div className="expense-buttons">
          <span
            className="expense-buttons-button"
            onClick={
              () => {
                this.props.setExtra(this.props.cost.extra - 1);
              }
            }
          >
            -
          </span>
          <span
            className="expense-buttons-button"
            onClick={
            () => {
              this.props.setExtra(this.props.cost.extra + 1);
            }
          }
          >
            +
          </span>
        </div>
      </>
    );
  }

  renderViewing() {
    return (
      <>
        <div className="expense-item-name">
          {this.props.cost.name}
          {this.props.cost.extra !== 0
            ? <span className="green bold">{`×${this.props.cost.extra + 1}`}</span> : undefined}
        </div>
        <div className={this.props.cost.cost > 0 ? 'green' : this.props.cost.cost < 0 ? 'red' : ''}>
          {formatMoney(this.props.cost.cost)}
        </div>
        {
        this.props.cost.delta_points === 0 ? undefined
          : (
            <div className="expense-item-point">
              <FontAwesomeIcon icon="utensils" />
              {' '}
              {
            this.props.cost.delta_points > 0
              ? `+${this.props.cost.delta_points}`
              : this.props.cost.delta_points
          }
            </div>
          )
      }
      </>
    );
  }

  render() {
    return (
      <div key={this.props.cost.name} className={`expense-item${this.props.cost.is_me ? ' expense-item-me' : ''}`}>
        {
          this.props.editing ? this.renderEditing() : this.renderViewing()
        }
      </div>
    );
  }
}
