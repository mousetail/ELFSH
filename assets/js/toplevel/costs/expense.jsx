import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatMoney, localize } from '../../util/localization';
import { formatDate } from '../../util/date';
import { submitExpenseChange } from './util';
import { ErrorView } from '../../components/error';
import { MoneyInput } from '../../generic/numberInput';
import { ExpensePerson } from './expensePerson';
import { InlineLoading } from '../../components/loading';

export class Expense extends React.Component {
  constructor() {
    super();

    this.state = {
      editing: false,
      error: undefined,
      loading: false,

      old_expense: undefined,
      options_expanded: false,
    };
  }

  submit(params = {}) {
    const pars = { ...this.props.expense, ...params };
    this.setState({
      loading: true,
    });
    submitExpenseChange(
      this.props.house_id,
      pars.id,
      pars.description,
      pars.amount,
      pars.paid_by,
      pars.payed_for,
      pars.deleted,
      this.props.maxEntries,
      pars.is_meal,
    ).then(
      (data) => {
        this.props.replaceData(data);
        this.setState(
          {
            loading: false,
            editing: false,
          },
        );
      },
    ).catch(
      (error) => {
        console.log(error);
        this.setState({
          error,
          loading: false,
        });
      },
    );
  }

  toggleMoreOptions() {
    this.setState({
      options_expanded: !this.state.options_expanded,
    });
  }

  setExtra(name, extra) {
    console.log(`set extra ${name}, ${extra}`);
    if (extra < -1) {
      extra = -1;
    }
    const costs = this.props.expense.costs.map((cost) => {
      if (cost.name === name) {
        return { ...cost, extra };
      }
      return cost;
    });
    this.props.onChange(
      'costs',
      costs,
    );
    this.props.onChange(
      'payed_for',
      {
        ...this.props.expense.payed_for,
        [name]: extra + 1,

      },
    );
  }

  renderDeletedExpense() {
    return (
      <div className="expense-header expense-hidden">
        <div className="expense-header-name">{this.props.expense.description}</div>

        <ErrorView error={this.state.error} />
        <div className="expense-header-item">{localize('cost_deleted')}</div>
        {
        this.props.expense.editable
          ? (
            <div className="expense-buttons">
              <span
                className="expense-buttons-button"
                onClick={
                              () => {
                                this.props.onChange('deleted', false);
                                this.submit({ deleted: false });
                              }
                            }
                role="button"
                tabIndex={0}
              >
                <FontAwesomeIcon icon="eye" />
              </span>
            </div>
          ) : null
      }
      </div>
    );
  }

  renderButtonArea() {
    let buttonArea;
    if (this.state.loading) {
      buttonArea = <InlineLoading />;
    } else if (this.state.editing) {
      buttonArea = (
        <div className="expense-header-item expense-header-buttons">
          <button onClick={this.submit.bind(this)}>
            {localize('cost_submit')}
          </button>
          <a
            href=""
            onClick={
          (ev) => {
            ev.preventDefault();
            Object.keys(this.state.old_expense).forEach(
              (key) => {
                this.props.onChange(key, this.state.old_expense[key]);
              },
            );
            this.setState({ editing: false });
          }
        }
          >
            {localize('cost_cancel')}
          </a>
        </div>
      );
    } else if (!this.props.expense.editable) {
      buttonArea = null;
    } else {
      buttonArea = (
        <div className="expense-header-item expense-buttons">
          <span
            className="expense-buttons-button"
            onClick={() => this.setState(
              {
                editing: true,
                old_expense: { ...this.props.expense },
              },
            )}
          >
            <FontAwesomeIcon icon="edit" />
          </span>
          <span
            className="expense-buttons-button"
            onClick={() => {
              this.props.onChange('deleted', true);
              this.submit({ deleted: true });
            }}
          >
            <FontAwesomeIcon icon="trash-alt" />
          </span>
        </div>
      );
    }
    return buttonArea;
  }

  renderMoreOptions() {
    if (this.state.options_expanded) {
      return (
        <>
          <div className="expense-header-item" onClick={this.toggleMoreOptions.bind(this)}>
            <FontAwesomeIcon icon="caret-down" />
            {localize('cost_more_options')}
          </div>
          <div>
            <label>
              <input
                type="checkbox"
                checked={this.props.expense.is_meal}
                onClick={(ev) => {
                  this.props.onChange('is_meal', ev.target.checked);
                }}
              />
              {localize('cost_is_meal')}
            </label>
          </div>
        </>
      );
    }

    return (
      <div
        className="expense-header-item"
        onClick={this.toggleMoreOptions.bind(this)}
      >
        <FontAwesomeIcon icon="caret-right" />
        {localize('cost_more_options')}
      </div>
    );
  }

  render() {
    if (this.props.expense.deleted) {
      return this.renderDeletedExpense();
    }

    return (
      <>
        <div className={`expense-header${this.state.editing ? ' expense-header-editing' : ''}`}>
          <div className="expense-header-name">
            {this.state.editing
              ? (
                <input
                  value={this.props.expense.description}
                  onChange={
                       (event) => this.props.onChange('description', event.target.value)
}
                />
              )
              : this.props.expense.description}
          </div>

          <ErrorView error={this.state.error} />
          <div className="expense-header-item">
            {formatDate(new Date(this.props.expense.date))}
          </div>
          <div className="expense-header-item">
            {this.state.editing
              ? (
                <MoneyInput
                  value={this.props.expense.amount}
                  onChange={
                  (value) => this.props.onChange('amount', value)
}
                />
              )
              : formatMoney(this.props.expense.amount)}
            {
              this.state.editing && this.renderMoreOptions()
            }
          </div>
          {
            this.renderButtonArea()

          }
        </div>
        {
          this.props.expense.costs.map(
            (j) => (
              <ExpensePerson
                cost={j}
                editing={this.state.editing}
                setExtra={(extra) => this.setExtra(j.name, extra)}
                key={j.name}
              />
            ),
          )

        }
        <hr />
      </>
    );
  }
}
