import React from 'react';
import { request } from '../../util/request';
import { localize } from '../../util/localization';
import { ErrorView } from '../../components/error';
import { Dropdown } from '../../generic/dropdown';
import { timezones } from '../../util/timezones';

export class GeneralSettingsScreen extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      loading: false,
      error: undefined,
    };

    this.submit = this.submit.bind(this);
  }

  submit(event) {
    event.preventDefault();
    if (this.state.loading) {
      return;
    }

    this.setState(
      {
        loading: true,
        error: undefined,
      },
    );

    request(`/api/set_house_info/${this.props.house_info.id}`,
      {
        name: this.props.house_info.name,
        closing_time: this.props.house_info.closing_time,
        editable_days: Number.parseInt(this.props.house_info.editable_days, 10),
        timezone: this.props.house_info.timezone,
      })
      .then(
        () => this.setState({ loading: false }),
      )
      .catch(
        (error) => {
          console.log(error);
          this.setState(
            {
              error,
              loading: false,
            },
          );
        },
      );
  }

  toDropdownElement(value, pre = []) {
    const keys = Object.keys(value);
    keys.sort();
    const formatDropdownElement = (i, j) => (j ? (`${i}/${j}`) : i);
    return keys.map(
      (i) => {
        const pre2 = pre.concat([i]);
        if (Object.keys(value[i]).length === 0) {
          return {
            value: formatDropdownElement(...pre2),
            name: i.replace('_', ' '),
          };
        }
        return {
          value: formatDropdownElement(...pre2),
          name: i.replace('_', ' '),
          type: 'category',
          contents: this.toDropdownElement(value[i], pre2),
        };
      },
    );
  }

  render() {
    return (
      <form>
        {
          this.state.loading ? <div>{localize('loading')}</div> : undefined
        }
        <ErrorView error={this.state.error} />
        <label className="inputLabel">
          {localize('settings_name')}
          <input
            value={this.props.house_info.name}
            onChange={(event) => this.props.setHouseInfo('name', event.target.value)}
          />
        </label>
        <p>{localize('settings_name_description')}</p>
        <label className="inputLabel">
          {localize('settings_endtime')}
          <input
            pattern="\d\d:\d\d"
            value={this.props.house_info.closing_time}
            onChange={(event) => this.props.setHouseInfo('closing_time', event.target.value)}
          />
        </label>
        <p>{localize('settings_endtime_description')}</p>
        <label className="inputLabel">
          {localize('settings_dayseditable')}
          <input
            type="number"
            min="0"
            max="60"
            value={this.props.house_info.editable_days}
            onChange={(event) => this.props.setHouseInfo('editable_days', event.target.value)}
          />
        </label>
        <p>{localize('settings_dayseditable_description')}</p>
        <label className="inputLabel">
          {localize('settings_timezone')}
          <Dropdown
            value={this.props.house_info.timezone}
            options={
              this.toDropdownElement(timezones)
            }
            onChange={(value) => this.props.setHouseInfo('timezone', value)}
          />
        </label>
        <p>{localize('settings_timezone_description')}</p>
        <button onClick={this.submit.bind(this)}>
          {localize('cost_submit')}
        </button>
      </form>
    );
  }
}
