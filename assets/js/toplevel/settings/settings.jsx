import React from 'react';

import { Nav } from '../../components/nav';
import { localize } from '../../util/localization';
import { Dropdown } from '../../generic/dropdown';
import { request } from '../../util/request';
import { ErrorView } from '../../components/error';
import { FullscreenLoading, InlineLoading } from '../../components/loading';
import { DeleteUserScreen } from './deleteUser';
import { UserConfig } from './userConfig';
import { GeneralSettingsScreen } from './generalSettingsScreen';

class InvitationForm extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      loading: false,
      name: '',
      email: '',
      language: localStorage.getItem('lang'),
      error: undefined,
    };

    this.submit = this.submit.bind(this);
  }

  submit(event) {
    event.preventDefault();
    this.setState(
      { loading: true },
    );
    request(`/api/send_invitation/${this.props.house_id}`,
      {
        name: this.state.name,
        email: this.state.email,
        lang: this.state.language,
      })
      .then(
        () => {
          this.setState({
            name: '',
            email: '',
            loading: false,
            error: undefined,
          });
        },
      )
      .catch(
        (error) => this.setState({
          error,
          loading: false,
        }),
      );
  }

  render() {
    return (
      <form>
        <ErrorView error={this.state.error} />
        <label className="inputLabel">
          {localize('settings_user_name')}
          <input
            value={this.state.name}
            onChange={(event) => this.setState({ name: event.target.value })}
          />
        </label>
        <br />
        <label className="inputLabel">
          {localize('settings_user_email')}
          <input
            type="email"
            value={this.state.email}
            onChange={(event) => this.setState({ email: event.target.value })}
          />
        </label>
        <br />
        <label className="inputLabel">
          {localize('setting_user_language')}
          <Dropdown
            value={this.state.language}
            options={
              [
                {
                  name: localize('setting_lang_english'),
                  value: 'en',
                },
                {
                  name: localize('setting_lang_dutch'),
                  value: 'nl',
                },
              ]
            }
            onChange={(value) => this.setState({ language: value })}
          />
        </label>
        <br />
        {
          this.state.loading ? <InlineLoading />
            : (
              <button
                onClick={this.submit.bind(this)}
              >
                {localize('settings_user_send')}
              </button>
            )
        }
      </form>
    );
  }
}

export default class Settings extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      loading: true,
      error: undefined,
    };

    this.setHouseInfo = this.setHouseInfo.bind(this);
    this.showDeleteModal = this.showDeleteModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  componentDidMount() {
    document.title = `${localize('page_settings')} - ELFSH`;

    this.reload();
  }

  reload() {
    request(`/api/info/${this.props.match.params.house_id}`)
      .then(
        // TODO: do something usefull with the data
        (data) => this.setState(
          {
            loading: false,
            data,
            error: undefined,
          },
        ),
      )
      .catch(
        (error) => {
          this.setState(
            {
              loading: false,
              error,
              data: undefined,
            },
          );
          console.error(error);
        },
      );
  }

  setHouseInfo(info, value) {
    this.setState(
      (state) => ({
        data: {
          ...state.data,
          house_info: {
            ...state.data.house_info,
            [info]: value,
          },
        },
      }),
    );
  }

  showDeleteModal(userId, userName) {
    this.setState({
      deleteModalVisible: true,
      deletingUserId: userId,
      deletingUsername: userName,
      error: undefined,
    });
  }

  hideModal(message) {
    if (message === undefined) {
      this.setState({
        deleteModalVisible: false,
      });
    }

    if (message.status === 'error') {
      this.setState({
        error: message.error,
        deleteModalVisible: false,
      });
    } else {
      this.setState({
        deleteModalVisible: false,
      });
      this.reload();
    }
  }

  render() {
    return (
      <>
        <Nav
          match={this.props.match}
          selected="settings"
        />
        {this.state.loading ? <FullscreenLoading />
          : this.state.error ? <ErrorView error={this.state.error} />
            : (
              <>

                <h1>
                  {`${localize('settings_title')} ${this.state.data.house_info.name}`}
                </h1>
                <h2>{localize('settings_general')}</h2>

                <GeneralSettingsScreen
                  house_info={this.state.data.house_info}
                  setHouseInfo={this.setHouseInfo}
                />
                <h2>{localize('settings_users')}</h2>
                <div className="settings-user-info-container">
                  {
                    this.state.data.user_info.map(
                      (i) => <UserConfig user={i} key={i.id} showModal={this.showDeleteModal} />,
                    )
                  }
                </div>
                <DeleteUserScreen
                  modalVisible={this.state.deleteModalVisible}
                  name={this.state.deletingUsername}
                  userId={this.state.deletingUserId}
                  houseId={this.props.match.params.house_id}
                  hideModal={this.hideModal}
                />

                <h2>{localize('settings_invitations')}</h2>
                <InvitationForm house_id={this.props.match.params.house_id} />

                <h2>{localize('settings_extra_options')}</h2>

                <a className="button" href="/api/export/7/2019-01-01">
                  {localize('settings_export_data')}
                </a>

                <div className="spacing" />
              </>
            )}
      </>
    );
  }
}
