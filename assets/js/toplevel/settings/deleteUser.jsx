import React from 'react';
import { request } from '../../util/request';

export class DeleteUserScreen extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit() {
    let result;
    try {
      result = await request(
        `/api/user/${this.props.houseId}/delete/${this.props.userId}/`,
        {},
      );
    } catch (ex) {
      this.props.hideModal(
        {
          status: 'error',
          error: ex,
        },
      );
      return;
    }

    this.props.hideModal(result);
  }

  render() {
    if (!this.props.modalVisible) {
      return <></>;
    }
    return (
      <div className="modal-lightbox">
        <div className="modal-content">
          <h1>
            Are you sure you want to delete
            &quot;
            {this.props.name}
            &quot;
          </h1>
          <button type="button" onClick={this.onSubmit}>Ok</button>
          <button type="button" onClick={this.props.hideModal}>Cancel</button>

        </div>
      </div>
    );
  }
}
