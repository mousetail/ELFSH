import React from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { get_cookie } from '../../util/cookies';

import { localize } from '../../util/localization.js';
import { FullscreenLoading } from '../../components/loading';

export class LanguageSelect extends React.Component {
  getHref(lang) {
    return `/${lang}/${this.props.match.url.split('/').slice(2).join('/')}`;
  }

  render() {
    return (
      <div className="language-select">
        <Link to={this.getHref('nl')}>
          <img
            src="/static/images/flags/nl.svg"
            height="100px"
            alt="Nederlands"
          />
        </Link>
        <Link to={this.getHref('en')}>
          <img
            src="/static/images/flags/en.svg"
            height="100px"
            alt="English"
          />
        </Link>
      </div>
    );
  }
}

export class LoginFragment extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      username: '',
      password: '',
    };
  }

  handleSubmit(event) {
    console.log(event);
    event.preventDefault();
    this.setState({ loading: true });
    fetch('/api/login/',
      {
        method: 'POST',
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
        }),
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': get_cookie('csrftoken'),
          accept: 'application/json',
        },
      }).then(
      (response) => response.json(),
    ).then(
      (data) => {
        if (data.status === 'error') {
          throw new Error(data.error);
        } else if (data.status === 'success') {
          return data;
        } else {
          throw new Error(`Ill-formatted response: ${JSON.stringify(data)}`);
        }
      },
    ).then(
      (data) => {
        this.props.onUserChange(
          true,
          data.user,
        );
        this.props.onSubmit(data);
      },
    )
      .catch(
        (error) => this.setState({ error: `${error}`, loading: false }),
      );
  }

  handleInputChange(event) {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;

    this.setState({
      [name]: value,
    });
  }

  render() {
    if (this.state.loading) {
      return <FullscreenLoading />;
    }
    return (
      <>
        <h1 id="login_head">{localize('login_welcome')}</h1>
        <LanguageSelect
          match={this.props.match}
        />
        {this.props.extraMessage ? <p>{this.props.extraMessage}</p> : undefined}
        <form onSubmit={this.handleSubmit.bind(this)}>
          {
            this.state.error ? <p>{this.state.error}</p> : undefined
          }
          <label className="inputLabel">
            {localize('login_username')}
            <input
              name="username"
              value={this.state.username}
              onChange={this.handleInputChange.bind(this)}
              id="username"
            />
          </label>
          <br />

          <label className="inputLabel">
            {localize('login_password')}
            <input
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.handleInputChange.bind(this)}
              id="password"
            />
          </label>
          <br />

          <button type="submit">{localize('login_login')}</button>
          <br />
          {this.props.showNewLink
            ? (
              <Link
                to={`/${this.props.match.params.lang}/new`}
                className="link-extra-margin"
                id="new-link"
              >
                {localize('login_create_account')}
              </Link>
            )
            : null}
          <Link
            to={`/${this.props.match.params.lang}/reset`}
            className="link-extra-margin"
            id="forgot-link"
          >
            {localize('login_forgot_password')}
          </Link>
        </form>
      </>
    );
  }
}

export default class LoginScreen extends React.Component {
  componentDidMount() {
    document.title = `ELFSH - ${localize('page_main')}`;
  }

  render() {
    if (this.props.loggedIn) {
      return <Redirect to={`/${this.props.match.params.lang}/houses`} />;
    }
    return (
      <>
        <h1>
          {localize('login_page_welcome')}
          <span className="subtitle">{localize('login_page_subtitle')}</span>
        </h1>
        <div className="hero">
          <img src="/static/images/eat_ill.svg" />
          <div>
            <p className="promo-header">{localize('promo_p_1')}</p>
            <div className="flex">
              <Link to={`/${this.props.match.params.lang}/new`} className="button">
                {localize('login_start')}
              </Link>
              <a className="button" href="#login_head">{localize('login_login')}</a>
            </div>
          </div>
        </div>
        <h1>{localize('promo_how')}</h1>

        <div className="main-page-card-container">
          <div className="card">
            <h2>{localize('promo_header_list')}</h2>
            <p>{localize('promo_p_list')}</p>
            <img
              src={`/static/images/promo/${this.props.match.params.lang}/list2.png`}
              alt={localize('screenshot_list')}
            />
          </div>
          <div className="card">
            <h2>{localize('promo_header_point')}</h2>
            <p>{localize('promo_p_point')}</p>
          </div>
          <div className="card">
            <h2>{localize('promo_header_cost')}</h2>
            <p>{localize('promo_p_cost')}</p>
            <img
              src={`/static/images/promo/${this.props.match.params.lang}/cost2.png`}
              alt={localize('screenshot_cost')}
            />
          </div>
          <div className="card">
            <h2>{localize('promo_header_cedit')}</h2>
            <p>{localize('promo_p_cedit')}</p>
            <img
              src={`/static/images/promo/${this.props.match.params.lang}/cost3.png`}
              alt={localize('screenshot_cost')}
            />
          </div>
          <div className="card">
            <h2>{localize('promo_header_6')}</h2>
            <p>{localize('promo_p_7')}</p>
            <img
              src={`/static/images/promo/${this.props.match.params.lang}/user.png`}
              alt={localize('screenshot_user')}
            />
          </div>
          <div className="card">
            <h2>{localize('promo_header_8')}</h2>
            <Link to={`/${this.props.match.params.lang}/new`}>{localize('login_create_account')}</Link>
          </div>
        </div>
        <h1>{localize('promo_compare')}</h1>

        <div className="table-container table-container-padding-top">
          <table cellSpacing={0} className="comparison-table">
            <tr>
              <th />
              <th>
                <div className="logo-container"><img src="/static/images/icon_128.png" alt="" /></div>
                ELFSH
              </th>
              <th>Eetlijst</th>
              <th>Monipal</th>
            </tr>
            <tr>
              <th>
                {localize('compare_time')}
                <p>{localize('compare_time_desc')}</p>
              </th>
              <td>
                2
                {localize('compare_click')}
                {' '}
                <br />
                {' '}
                7
                {' '}
                {localize('compare_second')}
              </td>
              <td>
                2
                {localize('compare_click')}
                {' '}
                <br />
                {' '}
                5
                {' '}
                {localize('compare_second')}
              </td>
              <td>
                5
                {localize('compare_click')}
                {' '}
                <br />
                {' '}
                10
                {' '}
                {localize('compare_second')}
              </td>
            </tr>
            <tr>
              <th>Open source?</th>
              <td>
                <FontAwesomeIcon icon="check" />
                <a href="https://gitlab.com/mousetail/ELFSH" rel="noopener noreferer">
                  {localize('compare_yes')}
                </a>
              </td>
              <td>
                <FontAwesomeIcon icon="times" />
                {localize('compare_no')}
              </td>
              <td>
                <FontAwesomeIcon icon="times" />
                {localize('compare_no')}
              </td>
            </tr>
            <tr>
              <th>
                {localize('compare_security')}
              </th>
              <td>
                <FontAwesomeIcon icon="check" />
                {localize('compare_account_pp')}
              </td>
              <td>
                <FontAwesomeIcon icon="exclamation-triangle" />
                {localize('compare_account_all')}
                <br />
                <FontAwesomeIcon icon="exclamation-triangle" />
                {localize('compare_account_nopassword')}
              </td>
              <td>
                <FontAwesomeIcon icon="check" />
                {localize('compare_account_pp')}
              </td>
            </tr>
            <tr>
              <th>
                {localize('compare_encryption')}
                <p>{localize('compare_encryption_detail')}</p>
              </th>
              <td>
                <a href="https://www.ssllabs.com/ssltest/analyze.html?d=elfsh.mousetail.nl">
                  A+
                </a>
              </td>
              <td>
                <a href="https://www.ssllabs.com/ssltest/analyze.html?d=eetlijst.nl">
                  {localize('compare_encryption_none')}
                </a>
              </td>
              <td><a href="https://www.ssllabs.com/ssltest/analyze.html?d=monipal.com">A</a></td>
            </tr>
          </table>
        </div>

        <LoginFragment
          onSubmit={() => this.props.history.push(`/${this.props.match.params.lang}/houses/`)}
          forceUpdate={() => this.forceUpdate()}
          onUserChange={this.props.onUserChange}
          showNewLink
          match={this.props.match}
        />

      </>
    );
  }
}
