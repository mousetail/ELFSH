import React from 'react';
import {
  BrowserRouter, Route, Switch, Redirect,
} from 'react-router-dom';

import { Userinfo } from './components/userinfo.jsx';

import { setLanguage, getLanguage, getDefaultLanguage } from './util/localization.js';
import { ErrorBoundary } from './components/errorBoundary';
import { Footer } from './components/footer';

import { FullscreenLoading } from './components/loading';

const login = React.lazy(() => import('./toplevel/login/login'));
const logout = React.lazy(() => import('./toplevel/login/logout'));
const houses = React.lazy(() => import('./toplevel/houses'));
const list = React.lazy(() => import('./toplevel/list'));
const costs = React.lazy(() => import('./toplevel/costs/costs'));
const settings = React.lazy(() => import('./toplevel/settings/settings'));

const invitationScreen = React.lazy(() => import('./toplevel/invitation'));
const acceptReset = React.lazy(() => import('./toplevel/reset/acceptReset'));
const requestReset = React.lazy(() => import('./toplevel/reset/requestReset'));
const newhouse = React.lazy(() => import('./toplevel/newhouse/newhouse'));
const newhouse_success = React.lazy(() => import('./toplevel/newhouse/newhouse_success'));
const verifyPage = React.lazy(() => import('./toplevel/verify'));

const errorPage = React.lazy(() => import('./toplevel/404'));

function renderWithProps(OtherComponent, props) {
  return (router_props) => <OtherComponent {...{ ...props, ...router_props }} />;
}

export class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loggedIn: false,
      user: undefined,
    };
  }

  onUserChange(loggedIn, user) {
    this.setState({
      loggedIn,
      user,
    });
  }

  getDefaultLanguage() {
    return getLanguage() || getDefaultLanguage();
  }

  updateLanguage(lang) {
    if (getLanguage() !== lang) {
      setLanguage(lang);
    }
    return null;
  }

  render() {
    const attrs = {
      loggedIn: this.state.loggedIn,
      user: this.state.user,
      onUserChange: this.onUserChange.bind(this),
    };

    return (
      <BrowserRouter>
        <ErrorBoundary>
          <div className="main-content">
            <Userinfo
              loggedIn={this.state.loggedIn}
              user={this.state.user}
              onUserChange={this.onUserChange.bind(this)}
            />

            <Route path="/en" render={() => this.updateLanguage('en')} />
            <Route path="/nl" render={() => this.updateLanguage('nl')} />
            <React.Suspense fallback={<FullscreenLoading />}>
              <Switch>
                <Route exact path="/" render={() => <Redirect to={`/${this.getDefaultLanguage()}/login`} />} />
                <Route
                  exact
                  path="/:lang([a-z]{2})/"
                  render={() => <Redirect to={`/${this.getDefaultLanguage()}/login`} />}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/login"
                  render={renderWithProps(login, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/logout"
                  render={renderWithProps(logout, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/houses"
                  render={renderWithProps(houses, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/list/:house_id(\d+)"
                  render={renderWithProps(list, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/cost/:house_id(\d+)"
                  render={renderWithProps(costs, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/settings/:house_id(\d+)"
                  render={renderWithProps(settings, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/invitation/:invitation_id"
                  render={(props) => (
                    <Redirect
                      match={props.match}
                      to={`/${props.match.params.lang}/invitation/${
                        props.match.params.invitation_id}/start`}
                      history={props.history}
                    />
                  )}

                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/invitation/:invitation_id/:page"
                  render={renderWithProps(invitationScreen, attrs)}
                />
                <Route
                  extact
                  path="/:lang([a-z]{2})/reset/:code"
                  render={renderWithProps(acceptReset, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/reset"
                  render={renderWithProps(requestReset, attrs)}
                />

                <Route
                  exact
                  path="/:lang([a-z]{2})/new"
                  render={renderWithProps(newhouse, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/new/done"
                  render={renderWithProps(newhouse_success, attrs)}
                />
                <Route
                  exact
                  path="/:lang([a-z]{2})/verify/:code"
                  render={renderWithProps(verifyPage, attrs)}
                />

                <Route exact render={renderWithProps(errorPage, attrs)} />
              </Switch>
            </React.Suspense>

            <Footer />
          </div>
        </ErrorBoundary>
      </BrowserRouter>
    );
  }
}
