let dataCacheName = 'ELFSH-pwa';
let cacheName = 'ELFSH-pwa';
let filesToCache = [
  "/static/css/style.css"
];

const version = 13;
const versionString = `[Service Worker v${version}] `;

function escapeHTML(str) {
  return str
    .replace(/&/g, '&amp;')
    .replace(/>/g, '&gt;')
    .replace(/</g, '&lt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&apos;');
}

function splitURL(url) {
  //console.log("splitting url: ", url);
  let protocol, uri, urz;
  [protocol, uri] = url.split("://");
  //console.log(`${versionString}protocol: ${protocol}, uri: ${uri}`);
  [host, ...urz] = uri.split("/");
  //console.log(host, urz);
  return {
    protocol: protocol,
    host: host,
    url: urz
  }
}

self.addEventListener('install', function (e) {
  console.log(versionString + 'Install');
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log(versionString + 'Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function (e) {
  console.log(versionString + 'Activate (V' + version + ')');
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});

function loadFromCache(ev, upev) {
  return caches.open(cacheName).then(
    cache => {
      return cache.match(ev.request).then(
        match => {
          if (match) {
            console.log(`${versionString} [${ev.request.url}] from cache `);
          } else {

            console.log(`${versionString} [${ev.request.url}] not in cache `);
          }
          return match || upev
        }
      )
    }
  )
}

function updateCache(ev) {
  return caches.open(cacheName).then(function (cache) {
    return fetch(ev.request).then(function (response) {

      console.log(`${versionString} [${ev.request.url}] save to cache `);
      return cache.put(ev.request, response.clone()).then(
        () => response
      );
    });
  });
}

self.addEventListener('fetch', function (e) {
  console.log(versionString + 'Fetch', e.request.url);

  const subpage = splitURL(e.request.url).url[0];


  if (subpage === "static") {
    const upcache = updateCache(e);


    e.respondWith(
      loadFromCache(e, upcache).then(
        res => console.log('res: ', e.request.url, res) || res
      )
    );

    e.waitUntil(upcache.then(
      res => console.log('res: ', e.request.url, res) || res
      )
    );

  } else if (subpage === "api") {
    e.respondWith(
      fetch(e.request)
    )
  } else {
    e.respondWith(
      fetch(e.request).catch((error) => {
          let content = `
    <html>
        <head>
            <link rel='stylesheet' href='/static/assets/bundles/style.css'>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        </head>
            <body>
            <div class='main-content'>
            <span class='errorMessage'>${escapeHTML("" + error.message)}</span>
            <a class="button" href="/?reload"><-</a>
        </div>
    </body>
    </html>`;
          console.log(versionString + "Preparing error...");
          return Promise.resolve(
            new Response(
              new Blob([content],
                {type: 'text/html'}
              ),
              {
                'status': 200,

              }
            )
          );
        }
      )
    )
  }
});

self.addEventListener('push', (event) => {
  console.log(versionString + "Received push event");
  let title, options;
  options = {};
  try {
    // Push is a JSON
    let response_json = event.data.json();

    console.log(versionString, JSON.stringify(response_json));
    title = response_json.title;
    options = response_json.options;
  } catch (err) {
    // Push is a simple text
    title = "ELFSH";
    options = {"body": event.data.text()}
  }

  //console.log(versionString+JSON.stringify(Object.keys(options)));
  self.registration.showNotification(
    title, options
  ).catch(
    (error) => console.error(versionString + error)
  )
  // Optional: Comunicating with our js application. Send a signal
  /*self.clients.matchAll({includeUncontrolled: true, type: 'window'}).then(function (clients) {
      clients.forEach(function (client) {
          client.postMessage({
              "data": message_tag,
              "data_title": title,
              "data_body": message
          });
      });
  });*/
});

// Optional: Added to that the browser opens when you click on the notification push web.
self.addEventListener('notificationclick', (event) => {
  event.notification.close();
  event.waitUntil(clients.matchAll({type: 'window', includeUncontrolled: true}).then(function (windowClients) {
      for (let i = 0; i < windowClients.length; i++) {
        let client = windowClients[i];
        if ('focus' in client) {
          return client.focus();
        }
      }
    })
  );
});