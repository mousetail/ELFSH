import React from 'react';

class DropdownElement extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
    };
  }

  render() {
    let category;
    if (this.props.element.type === 'category') {
      category = true;
    }

    let contents;
    if (category && this.state.open) {
      contents = (
        <div className="dropdown-category-container">
          {this.props.element.contents.map(
            (i) => (
              <DropdownElement
                key={i.value}
                element={i}
                selected={this.props.selected}
                onChange={this.props.onChange}
              />
            ),
          )}
        </div>
      );
    } else {
      contents = undefined;
    }

    return (
      <>
        <div
          className={
                        `dropdown-element${
                          this.props.selected === this.props.element.value
                            ? ' dropdown-element-selected' : ''
                        }${category
                          ? ' dropdown-category' : ''}`
                    }
          key={this.props.element.value}
          onClick={() => {
            if (category) {
              this.setState({ open: !this.state.open });
            } else {
              this.props.onChange(this.props.element.value);
            }
          }}
        >

          {this.props.element.name}
        </div>

        {contents}
      </>
    );
  }
}

export class Dropdown extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
    };
  }

  get_selected() {
    const v = this.props.options.filter((i) => i.value === this.props.value);
    if (v.length === 1) {
      return v[0].name;
    }
    return this.props.value;
  }

  render() {
    return (
      <span className="dropdown">
        <div
          className="dropdown-main"
          onClick={
                    () => this.setState((state) => ({ open: !state.open }))
                }
        >
          {this.get_selected()}
        </div>

        {
                    this.state.open && (
                    <div className="dropdown-items">
                        {
                            this.props.options.map(
                              (i) => (
                                <DropdownElement
                                  element={i}
                                  selected={this.props.value}
                                  onChange={(value) => {
                                    this.props.onChange(value);
                                    this.setState({ open: false });
                                  }}
                                  key={i.value}
                                />
                              ),
                            )
                        }
                    </div>
                    )
                }
      </span>
    );
  }
}
