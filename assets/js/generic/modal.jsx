import React from 'react';

export class Modal extends React.Component {
  render() {
    return (
      <div className="modal-lightbox">
        <div className="modal-content">
          {this.props.children}
        </div>
      </div>
    );
  }
}
